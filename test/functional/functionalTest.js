"use stricts";
let chai = require("chai");
let chaiHttp = require("chai-http");
const expect = require("chai").expect;

chai.use(chaiHttp);
const url = "http://localhost:3000/stg";
const validId = "86afadc8-fde0-4472-a3f8-b77894cc3971";
const invalidId = "egfhjgksfdjkh5454";
let newPostId = "";

describe("Insert a car: ", () => {
  it("should insert a car", (done) => {
    chai
      .request(url)
      .post("/cars")
      .send({
        brand: "vw",
        model: "gol",
        engine: "1600cc",
        make: 2009,
      })
      .end(function (err, res) {
        newPostId = res.body.id;
        console.log(res.body);
        if (err) {
          console.log(err.message);
        }
        expect(res).to.have.status(200);
        done();
      });
  });
});

describe("Insert a car with error: ", () => {
  it("should receive an error", (done) => {
    chai
      .request(url)
      .post("/cars")
      .send({
        brand: "vw",
        model: "gol",
        engine: "1600",
        make: 2025,
      })
      .end(function (err, res) {
        console.log(res.body);
        if (err) {
          console.log(err.message);
        }
        expect(res).to.have.status(500);
        done();
      });
  });
});

describe("get a car: ", () => {
  it("should get a car", (done) => {
    chai
      .request(url)
      .get("/cars/" + validId)
      .end(function (err, res) {
        console.log(res.body);
        if (err) {
          console.log(err.message);
        }
        expect(res.body).to.have.property("id").to.be.equal(validId);
        expect(res).to.have.status(200);
        done();
      });
  });
});

describe("get a car with error: ", () => {
  it("should receive an error", (done) => {
    chai
      .request(url)
      .get("/cars/" + invalidId)
      .end(function (err, res) {
        console.log(res.body);
        if (err) {
          console.log(err.message);
        }
        expect(res).to.have.status(500);
        done();
      });
  });
});

describe("update a car", () => {
  it("should update a car", (done) => {
    chai
      .request(url)
      .put("/cars/" + validId)
      .send({
        engine: "2000cc",
        make: 1995,
      })
      .end(function (err, res) {
        console.log(res.body);
        if (err) {
          console.log(err.message);
        }
        expect(res.body).to.have.property("engine").to.be.equal("1400cc");
        expect(res.body).to.have.property("make").to.be.equal(2008);
        expect(res).to.have.status(200);
        done();
      });
  });
});

describe("update a car with error", () => {
  it("should recieve an error", (done) => {
    chai
      .request(url)
      .put("/cars/" + validId)
      .send({ engine: "1400", make: 2025 })
      .end(function (err, res) {
        console.log(res.body);
        if (err) {
          console.log(err.message);
        }
        expect(res).to.have.status(500);
        done();
      });
  });
});

describe("delete a car", () => {
  it("should delete a car", (done) => {
    chai
      .request(url)
      .del("/country/" + newPostId)
      .end(function (err, res) {
        console.log(res.body);
        if (err) {
          console.log(err.message);
        }
        expect(res).to.have.status(200);
      });
  });
});
