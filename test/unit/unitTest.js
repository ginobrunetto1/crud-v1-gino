const {
  postCarBodySchema,
  updateCarBodySchema,
  carIdSchema,
} = require("../../src/application/schemas/car.schema");
const chai = require("chai");
const expect = chai.expect;
const uuid = require("uuid-random");

describe("id, post and update body validation", function () {
  it("should fail invalid id", async function () {
    const id = 504121;
    let result;
    try {
      result = await carIdSchema.validateAsync(id);
    } catch (err) {
      result = err.message;
    }
    expect(result).to.be.a("string");
    expect(result).to.be.equal(504121);
  });
  it("should be Successful valid id", async function () {
    const id = uuid();
    let result;
    try {
      result = await carIdSchema.validateAsync(id);
    } catch (err) {
      result = err.message;
    }
    expect(result).to.be.a("string");
    expect(result).to.be.equal(id);
  });
  it("should fail invalid post body", async function () {
    const body = {
      brand: "vw",
      model: "gol power",
      engine: "1600",
      make: 2025,
    };
    let result;
    try {
      result = await postCarBodySchema.validateAsync(body);
    } catch (err) {
      result = err.message;
    }
    expect(result).to.be.equal({
      brand: "vw",
      model: "gol power",
      engine: "1600",
      make: 2025,
    });
  });
  it("should be Successful valid post body", async function () {
    const body = {
      brand: "vw",
      model: "gol power",
      engine: "1600cc",
      make: 2009,
    };
    let result;
    try {
      result = await postCarBodySchema.validateAsync(body);
    } catch (err) {
      result = err.message;
    }
    expect(result).to.be.equal({
      brand: "vw",
      model: "gol power",
      engine: "1600cc",
      make: 2009,
    });
  });
  it("should fail invalid update body", async function () {
    const body = {
      engine: "1600",
      make: 2025,
    };
    let result;
    try {
      result = await updateCarBodySchema.validateAsync(body);
    } catch (err) {
      result = err.message;
    }
    expect(result).to.be.equal({
      engine: "1600",
      make: 2025,
    });
  });
  it("should be Successful valid update body", async function () {
    const body = {
      engine: "1600cc",
      make: 2009,
    };
    let result;
    try {
      result = await updateCarBodySchema.validateAsync(body);
    } catch (err) {
      result = err.message;
    }
    expect(result).to.be.equal({
      engine: "1600cc",
      make: 2009,
    });
  });
});
