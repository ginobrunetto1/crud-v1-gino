"use strict";
const AWS = require("aws-sdk");
const dynamoDb = new AWS.DynamoDB.DocumentClient();
var uuid = require("uuid-random");

async function scan(tableName) {
  const params = {
    TableName: tableName,
  };

  return dynamoDb.scan(params).promise();
}

async function get(tableName, id) {
  const params = {
    TableName: tableName,
    Key: {
      id: id,
    },
  };

  return dynamoDb.get(params).promise();
}

async function post(tableName, body) {
  const objectBody = body;
  objectBody.id = objectBody.id || uuid();
  const params = {
    Item: objectBody,
    TableName: tableName,
  };
  await dynamoDb.put(params).promise();
}

async function update(tableName, body, id) {
  const generateUpdateQuery = (fields) => {
    let exp = {
      UpdateExpression: "set",
      ExpressionAttributeValues: {},
    };
    Object.entries(fields).forEach(([key, item]) => {
      exp.UpdateExpression += ` ${key} = :${key},`;
      exp.ExpressionAttributeValues[`:${key}`] = item;
    });
    exp.UpdateExpression = exp.UpdateExpression.slice(0, -1);
    return exp;
  };

  let expression = generateUpdateQuery(body);

  const params = {
    TableName: tableName,
    Key: {
      id,
    },
    UpdateExpression: expression.UpdateExpression,
    ExpressionAttributeValues: expression.ExpressionAttributeValues,
  };
  await dynamoDb.update(params).promise();
}

async function remove(tableName, id) {
  const params = {
    TableName: tableName,
    Key: {
      id: id,
    },
  };
  const result = await dynamoDb.delete(params).promise();
}

module.exports = {
  get,
  scan,
  post,
  update,
  remove,
};
