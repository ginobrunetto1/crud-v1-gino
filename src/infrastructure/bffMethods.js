const superagent = require("superagent");

const path = process.env.BFF_PATH + "cars/"

async function post(body) {
  await superagent.post(path).send(body);
  return body;
}

async function update(body, id) {
  await superagent.put(path + id).send(body);
  return body;
}

async function get(id) {
  return await superagent.get(path + id);
}

module.exports = { post, update, get };
