const AWS = require("aws-sdk");
const sns = new AWS.SNS({
  region: "us-east-1", // Set the region in which SES is configured
});

const publishNotification = async (message, subject, topicArn) => {
  var params = {
    Message: message,
    Subject: subject,
    TopicArn: topicArn,
  };
  await sns.publish(params).promise();
};

module.exports = {
  publishNotification,
};
