const AWS = require("aws-sdk");
const ses = new AWS.SES();

async function newTemplate(templateName, emailBody, emailSubject, emailText) {
  const params = {
    Template: {
      TemplateName: templateName,
      HtmlPart: emailBody,
      SubjectPart: emailSubject,
      TextPart: emailText,
    },
  };
  return await ses.createTemplate(params).promise();
}

async function removeTemplate(templateName) {
  const params = {
    TemplateName: templateName,
  };
  return await ses.deleteTemplate(params).promise();
}

async function sendEmail(templateName, toAddress, sourceAddress, templateData) {
  const params = {
    Destination: {
      ToAddresses: toAddress,
    },
    Source: sourceAddress,
    Template: templateName,
    TemplateData: templateData,
  };
  return await ses.sendTemplatedEmail(params).promise();
}

module.exports = {
  newTemplate,
  removeTemplate,
  sendEmail,
};
