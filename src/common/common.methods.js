function lowerCase(obj) {
  Object.entries(obj).forEach(([key, item]) => {
    if (typeof item == "string" && key !== "id") {
      obj[key] = item.toLowerCase();
    }
  });
}

function firstUpperCase(obj) {
  Object.entries(obj).forEach(([key, item]) => {
    if (typeof item == "string" && key !== "id") {
      obj[key] = item.toLowerCase().replace(item.charAt(0), item.charAt(0).toUpperCase());
    }
  });
}

module.exports = { lowerCase, firstUpperCase };
