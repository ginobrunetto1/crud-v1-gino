const func = require("../../domain/bffDomain");
const { validationBffPostCarBody } = require("../schemas/car.schema");

async function bffPost(req, res) {
  const validationIdResult = validationBffPostCarBody(req.body);

  if (validationIdResult.error) {
    res.status(400).json(validationIdResult.error);
  } else {
    try {
      const result = await func.bffSwitch(req.body);
      res.status(200).json(result);
    } catch (err) {
      res.status(500).json({
        error: "Could not post/update a car",
        details: err.toString(),
      });
    }
  }
}

module.exports = { bffPost };
