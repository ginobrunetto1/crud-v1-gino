"use strict";
const func = require("../../infrastructure/dydbMethods");
const dbName = process.env.CAR_TABLE_NAME;
const {
  validationCarId,
  validationPostCarBody,
  validationUpdateCarBody,
} = require("../schemas/car.schema");

async function scanCars(req, res) {
  try {
    const result = await func.scan(dbName);
    res.status(200).json(result);
  } catch (err) {
    res
      .status(500)
      .json({ error: "Could not get a car", details: err.toString() });
  }
}

async function getCar(req, res) {
  const validationIdResult = validationCarId(req.params.id);

  if (validationIdResult.error) {
    res.status(400).json(validationIdResult.error);
  } else {
    try {
      const result = await func.get(dbName, req.params.id);
      res.status(200).json(result);
    } catch (err) {
      res
        .status(500)
        .json({ error: "Could not get a car", details: err.toString() });
    }
  }
}

async function postCar(req, res) {
  const validationBodyResult = validationPostCarBody(req.body);

  if (validationBodyResult.error) {
    res.status(400).json(validationBodyResult.error);
  } else {
    try {
      await func.post(dbName, req.body);
      res.status(200).json({ message: "Post succesfull" });
    } catch (err) {
      res
        .status(500)
        .json({ error: "Could not post a car", details: err.toString() });
    }
  }
}

async function updateCar(req, res) {
  const validationBodyResult = validationUpdateCarBody(req.body);
  const validationIdResult = validationCarId(req.params.id);

  if (validationBodyResult.error || validationIdResult.error) {
    const error = [];
    if (validationBodyResult.error) {
      error.push(validationBodyResult.error);
    }
    if (validationIdResult.error) {
      error.push(validationIdResult.error);
    }
    res.status(400).json(error);
  } else {
    try {
      await func.update(dbName, req.body, req.params.id);
      res.status(200).json({ message: "Update succesfull" });
    } catch (err) {
      res
        .status(500)
        .json({ error: "Could not update a car", details: err.toString() });
    }
  }
}

async function removeCar(req, res) {
  const validationIdResult = validationCarId(req.params.id);

  if (validationIdResult.error) {
    res.status(400).json(validationIdResult.error);
  } else {
    try {
      await func.remove(dbName, req.params.id);
      res.status(200).json({ message: "Delete succesfull" });
    } catch (err) {
      res
        .status(500)
        .json({ error: "Could not delete a car", details: err.toString() });
    }
  }
}

module.exports = { getCar, postCar, updateCar, removeCar, scanCars };
