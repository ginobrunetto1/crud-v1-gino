"use strict";
const { publishNotification } = require("../../infrastructure/snsService");

module.exports.notificationSender = async (event, context, callback) => {
  try {
    await publishNotification(
      "Algo paso con tu BD",
      "Alerta!!",
      process.env.TOPIC_ARN
    );
    callback(null, { statusCode: 200 });
  } catch (err) {
    callback(null, {
      statusCode: 500,
      body: JSON.stringify({
        code: "internal_server_error",
        severity: "HIGH",
        message: "Internal Server Error",
        details: err.toString(),
      }),
    });
  }
};
