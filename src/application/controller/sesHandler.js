"use strict";
const { emailSender } = require("../../domain/sesDomain");

module.exports.sendEmail = async (event, context, callback) => {
  try {
    await emailSender();
    callback(null, { statusCode: 200 });
  } catch (err) {
    callback(err, {      
      body: JSON.stringify({
        details: err.toString(),
      }),
    });
  }
};