const Joi = require("@hapi/joi");

const updateObj = {
  brand: Joi.string().alphanum().min(1).max(30),
  model: Joi.string().alphanum().min(1).max(30),
  engine: Joi.string().pattern(new RegExp("^[0-9]{3,4}[a-z]{2}$")),
  make: Joi.number().integer().min(1900).max(2020),
};

const postObj = {};
Object.keys(updateObj).forEach((key) => {
  postObj[key] = updateObj[key].required();
});
postObj.id = Joi.string().min(1);

const postCarBodySchema = Joi.object(postObj);

const postBffObj = postObj;
postBffObj.id = postBffObj.id.required();

const postBffCarBodySchema = Joi.object(postBffObj);

const updateCarBodySchema = Joi.object(updateObj);

const carIdSchema = Joi.string().min(1).required();

const validationPostCarBody = (payload) => {
  try {
    Joi.assert(payload, postCarBodySchema);
    return payload;
  } catch (err) {
    const error = new Error();
    error.code = "bad_request";
    error.severity = "HIGH";
    error.message = err.message;
    payload.error = error;
    return payload;
  }
};

const validationBffPostCarBody = (payload) => {
  try {
    Joi.assert(payload, postBffCarBodySchema);
    return payload;
  } catch (err) {
    const error = new Error();
    error.code = "bad_request";
    error.severity = "HIGH";
    error.message = err.message;
    payload.error = error;
    return payload;
  }
};

const validationUpdateCarBody = (payload) => {
  try {
    Joi.assert(payload, updateCarBodySchema);
    return payload;
  } catch (err) {
    const error = new Error();
    error.code = "bad_request";
    error.severity = "HIGH";
    error.message = "Validation Schema Failed";
    payload.error = error;
    return payload;
  }
};

const validationCarId = (payload) => {
  try {
    Joi.assert(payload, carIdSchema);
    return payload;
  } catch (err) {
    const error = new Error();
    error.code = "bad_request";
    error.severity = "HIGH";
    error.message = "Validation Schema Failed";
    payload.error = error;
    return payload;
  }
};

module.exports = {
  validationPostCarBody,
  validationUpdateCarBody,
  validationBffPostCarBody,
  validationCarId,
  postCarBodySchema,
  updateCarBodySchema,
  postBffCarBodySchema,
  carIdSchema,
};
