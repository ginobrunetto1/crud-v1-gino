"use strict";
const serverless = require("serverless-http");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const {
  getCar,
  postCar,
  updateCar,
  removeCar,
  scanCars,
} = require("../controller/dydbHandler");
const { bffPost } = require("../controller/bffHandler");

app.use(bodyParser.json({ strict: false }));

app.get("/cars", scanCars);
app.get("/cars/:id", getCar);
app.post("/cars", postCar);
app.put("/cars/:id", updateCar);
app.delete("/cars/:id", removeCar);
app.post("/bff/car", bffPost);

module.exports.handler = serverless(app);
