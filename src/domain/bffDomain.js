const bff = require("../infrastructure/bffMethods");
const { lowerCase, firstUpperCase } = require("../common/common.methods");

async function bffSwitch(body) {
  lowerCase(body);
  const idExist = await bff.get(body.id);
  if (idExist.body.id) {
    const result = await bff.update(body);
    firstUpperCase(result)
    return result;
  } else {
    const result = await bff.post(body);
    firstUpperCase(result);
    return result;
  }
}

module.exports = { bffSwitch };
