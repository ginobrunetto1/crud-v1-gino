const ses = require("../infrastructure/sesService");
var Chance = require("chance");
var chance = new Chance();
const AWS = require("aws-sdk");
const s3 = new AWS.S3();


const getTemplate = async (bucketName, fileName) => {
  let params = {
    Bucket: bucketName,
    Key: fileName,
  };
  const data = await s3.getObject(params).promise();
  return data.Body.toString();
};

async function emailSender() {
  const emailBody = await getTemplate(process.env.BUCKET_NAME, process.env.FILE_NAME);
  const templateName = chance.hash({ length: 15 });
  const toAddress = [process.env.TO_ADDRESS];
  try {
    await ses.newTemplate(
      templateName,
      emailBody,
      process.env.EMAIL_SUBJECT,
      process.env.EMAIL_TEXT
    );
    await ses.sendEmail(
      templateName,
      toAddress,
      process.env.SOURCE_ADDRESS,
      process.env.TEMPLATE_DATA
    );
    await ses.removeTemplate(templateName);
  } catch (err) {
    console.log(err.toString());
  }
}

module.exports = { emailSender };
